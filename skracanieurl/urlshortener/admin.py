from django.contrib import admin

from .models import ShortenedURL
# Register your models here.

class ShortenedURLAdmin(admin.ModelAdmin):
	list_display = ('original_url', 'short_url')

admin.site.register(ShortenedURL, ShortenedURLAdmin)