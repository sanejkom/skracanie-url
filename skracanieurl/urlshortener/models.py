from django.db import models

# Create your models here.

class ShortenedURL(models.Model):
    original_url = models.CharField(max_length=200)
    short_url = models.CharField(max_length=20)
    def __str__(self):
        return self.original_url