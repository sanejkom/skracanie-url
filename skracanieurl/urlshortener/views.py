from django.shortcuts import get_object_or_404, render
from django.http import HttpResponse, HttpResponseRedirect
import random, string
from .models import ShortenedURL

# Create your views here.

def index(request):
    return render(request, 'urlshortener/index.html')

def redirect(request, shortenedurl):
    url = get_object_or_404(ShortenedURL, short_url = shortenedurl)
    return HttpResponseRedirect(url.original_url)

def generate(request):
    url = request.POST.get("originalurl", '')
    if not (url == ''):
        char = string.ascii_uppercase + string.digits + string.ascii_lowercase
        while True:
            shorturl = ''.join(random.choice(char) for i in range(6))
            try:
                temp = ShortenedURL.objects.get(short_url = shorturl)
            except:
                break
        newurl = ShortenedURL(original_url = url, short_url = shorturl)
        newurl.save()
        context = {'newurl': newurl}
        return render(request, 'urlshortener/result.html', context)